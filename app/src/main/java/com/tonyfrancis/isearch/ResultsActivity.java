package com.tonyfrancis.isearch;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.SearchView;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.apache.commons.io.IOUtils;

import static android.app.PendingIntent.getActivity;


public class ResultsActivity extends ActionBarActivity implements RecyclerItemClickListener.OnItemClickListener {
    protected static ArrayList<SearchResult> searchResults;
    protected String query;
    private RecyclerView recyclerView;
    private ViewAdapter adapter;
    private SearchView searchView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        recyclerView = (RecyclerView) findViewById(R.id.list);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(gridLayoutManager);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) findViewById(R.id.searchView2);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        handleIntent(getIntent());

    }


    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            searchResults = new ArrayList<>();
            query = handleQuery(intent.getStringExtra(SearchManager.QUERY));
            new MyAsyncTask().execute();
        }

    }

    private String handleQuery(String query) {
        query = query.toLowerCase();
        query = query.trim();
        query = query.replaceAll(" ", "+");
        return query;
    }

    private void buildLayout() {
        adapter = new ViewAdapter(this, searchResults);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(this, this));
    }

    @Override
    public void onItemClick(View childView, int position) {
        System.out.println("Activity for position" + position);
        Intent intent = new Intent(ResultsActivity.this, AlbumActivity.class);
        intent.putExtra("POSITION", position);
        startActivity(intent);
    }

    @Override
    public void onItemLongPress(View childView, int position) {

    }


    private class MyAsyncTask extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            System.out.println(query);
            DefaultHttpClient httpClient = new DefaultHttpClient(new BasicHttpParams());
            HttpPost httpPost = new HttpPost("https://itunes.apple.com/search?term=" + query + "&entity=album");

            httpPost.setHeader("Content-type", "application/json");
            InputStream inputStream = null;

            String result = null;

            try {
                HttpResponse response = httpClient.execute(httpPost);
                HttpEntity entity = response.getEntity();
                inputStream = entity.getContent();


                result = IOUtils.toString(inputStream, "UTF-8");
                inputStream.close();
                System.out.println(result);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {

                try {
                    if (inputStream != null)
                        inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
            JSONObject jsonObject;
            JSONArray jsonArray = null;
            try {
                jsonObject = new JSONObject(result);
                jsonArray = jsonObject.getJSONArray("results");
            } catch (JSONException e) {
                e.printStackTrace();
            }

        if (jsonArray != null) {
            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    JSONObject obj = jsonArray.getJSONObject(i);
                    searchResults.add(new SearchResult(obj, i));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }

        return null;
        }

        @Override
        protected void onPostExecute(String result) {
            buildLayout();
        }

    }


}
