package com.tonyfrancis.isearch;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.View;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URL;

/**
 * Created by tonyfrancis on 4/2/15.
 */
public class SearchResult {
    private JSONObject album;
    private int iconId;
    private Bitmap bitmapIcon;
    private Bitmap bitmapImage;
    private int position;



    public SearchResult(JSONObject album, final int position) {
        this.album = album;
        this.position = position;
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    InputStream in = new URL(getArtworkUrlSmall()).openStream();
                    bitmapIcon = BitmapFactory.decodeStream(in);
                    in.close();
                } catch (Exception e) {
                    // log error
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                if (position < ViewAdapter.holders.size())
                    ViewAdapter.holders.get(position).updateImage(bitmapIcon);
            }

        }.execute();

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    InputStream in = new URL(getArtworkUrlLarge()).openStream();
                    bitmapImage = BitmapFactory.decodeStream(in);
                    in.close();
                } catch (Exception e) {
                    // log error
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void result) {
                AlbumActivity.updateImage();
            }

        }.execute();

    }

    public String getCollectionId() {
        String result = "";
        try {
            result = album.getString("collectionId");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getCollectionName() {
        String result = "";
        try {
            result = album.getString("collectionName");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getCollectionViewUrl() {
        String result = "";
        try {
            result = album.getString("collectionViewUrl");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getArtworkUrlSmall() {
        String result = "";
        try {
            result = album.getString("artworkUrl100");
            result = result.replace("100x100-75", "152x152-75");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getArtworkUrlLarge() {
        String result = "";
        try {
            result = album.getString("artworkUrl100");
            result = result.replace("100x100-75", "512x512-75");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getArtistName() {
        String result = "";
        try {
            result = album.getString("artistName");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getPrice() {
        String result = "";
        try {
            result = album.getString("collectionPrice");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public String getGenre() {
        String result = "";
        try {
            result = album.getString("primaryGenreName");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public Bitmap getIcon() {
        return bitmapIcon;
    }
    public Bitmap getImage() {
        return bitmapImage;
    }


}
