package com.tonyfrancis.isearch;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by tonyfrancis on 4/3/15.
 */
public class ViewAdapter extends RecyclerView.Adapter<ViewAdapter.MyViewHolder> {

    protected static ArrayList<MyViewHolder> holders = new ArrayList<>();
    private LayoutInflater inflator;
    private List<SearchResult> data = Collections.emptyList();

    public ViewAdapter(Context context, List<SearchResult> data) {
        this.data = data;
        inflator = LayoutInflater.from(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflator.inflate(R.layout.custom_grid, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        SearchResult current = data.get(position);

        holder.setAlbum(current);
        holder.image.setImageBitmap(current.getIcon());
        holder.text.setText(current.getCollectionName());
        holders.add(position, holder);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    protected class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView image;
        TextView text;
        SearchResult album;

        public MyViewHolder(View itemView) {
            super(itemView);

            image = (ImageView) itemView.findViewById(R.id.imageView2);
            text = (TextView) itemView.findViewById(R.id.textView);

        }

        private void setAlbum(SearchResult album) {
            this.album = album;
        }

        public void updateImage(Bitmap image) {
            this.image.setImageBitmap(image);
        }

        @Override
        public void onClick(View v) {}
    }


}
