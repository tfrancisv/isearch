package com.tonyfrancis.isearch;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Hashtable;


public class AlbumActivity extends ActionBarActivity {

    private TextView albumName;
    private TextView artist;
    private TextView genre;
    private Button button;
    private static ImageView image;
    private static SearchResult album;
    private Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        album = ResultsActivity.searchResults.get((int) getIntent().getSerializableExtra("POSITION"));
        setContentView(R.layout.activity_album);

        toolbar = (Toolbar) findViewById(R.id.toolbarAlbum);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        albumName = (TextView) findViewById(R.id.textViewAlbum);
        artist = (TextView) findViewById(R.id.textViewArtist);
        genre = (TextView) findViewById(R.id.textViewGenre);
        image = (ImageView) findViewById(R.id.imageView);

        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (album != null) {
                    Intent browserIntent = new Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse(album.getCollectionViewUrl()));
                    startActivity(browserIntent);
                }
            }
        });

        setAlbumContent();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.menu_album, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setAlbumContent() {
        albumName.setText(album.getCollectionName());
        artist.setText(album.getArtistName());
        genre.setText(album.getGenre());
        button.setText(album.getPrice() + " on iTunes");
        image.setImageBitmap(album.getImage());

    }

    public static void updateImage() {
        if (album != null) {
            image.setImageBitmap(album.getImage());
        }
    }
}
